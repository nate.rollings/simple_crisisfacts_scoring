example_gold_facts = {
    
    #Query: What are the wind speeds?
    "CrisisFACTS-General-q013": {
        "CrisisFACTS-001-r3": [r"20 (mph|miles per hour)"],
        "CrisisFACTS-001-r4": [r"12 (mph|miles per hour)"],
        "CrisisFACTS-001-r5": [r"7 (mph|miles per hour)"],
        "CrisisFACTS-001-r6": [r"8 (mph|miles per hour)"],
        "CrisisFACTS-001-r7": [r"12 (mph|miles per hour)"],
        "CrisisFACTS-001-r8": [r"8 (mph|miles per hour)"],
        "CrisisFACTS-001-r9": [r"7 (mph|miles per hour)"]
    },

    #Query: What areas are being evacuated - Maybe
    "CrisisFACTS-General-q014": {
        "CrisisFACTS-001-r3": [r"(W\.?|west) Lilac([^\n])*Sullivan Middle School|Sullivan Middle School([^\n])*(W\.?|west) Lilac", 
                               r"(S\.?|south) of Reche([^\n])*(W\.?|west) of I(-| |)15|(W\.?|west) of I(-| |)15([^\n])*(S\.?|south) of Reche",
                               r"(S\.?|south) of Burma([^\n])*(N\.?|north) of (N\.?|north) River|(N\.?|north) of (N\.?|north) River([^\n])*(S\.?|south) of Burma",
                               r"(E\.?|east) of (Green Canyon|(S\.?|south) Mission)([^\n])*(N\.?|north) of (hwy\.?|highway) 76|(N\.?|north) of (hwy\.?|highway) 76([^\n])*(E\.?|east) of (Green Canyon|(S\.?|south) Mission)"],
        "CrisisFACTS-001-r4": [r"(W\.?|west) Lilac([^\n])*Sullivan Middle School|Sullivan Middle School([^\n])*(W\.?|west) Lilac", 
                               r"(S\.?|south) of Reche([^\n])*(W\.?|west) of I(-| |)15|(W\.?|west) of I(-| |)15([^\n])*(S\.?|south) of Reche",
                               r"(S\.?|south) of Burma([^\n])*(N\.?|north) of (N\.?|north) River|(N\.?|north) of (N\.?|north) River([^\n])*(S\.?|south) of Burma",
                               r"(E\.?|east) of (Green Canyon|(S\.?|south) Mission)([^\n])*(N\.?|north) of (hwy\.?|highway) 76|(N\.?|north) of (hwy\.?|highway) 76([^\n])*(E\.?|east) of (Green Canyon|(S\.?|south) Mission)"],
        "CrisisFACTS-001-r5": [r"(W\.?|west) Lilac([^\n])*Sullivan Middle School|Sullivan Middle School([^\n])*(W\.?|west) Lilac", 
                               r"(S\.?|south) of Reche([^\n])*(W\.?|west) of I(-| |)15|(W\.?|west) of I(-| |)15([^\n])*(S\.?|south) of Reche",
                               r"(S\.?|south) of Burma([^\n])*(N\.?|north) of (N\.?|north) River|(N\.?|north) of (N\.?|north) River([^\n])*(S\.?|south) of Burma",
                               r"(E\.?|east) of (Green Canyon|(S\.?|south) Mission)([^\n])*(N\.?|north) of (hwy\.?|highway) 76|(N\.?|north) of (hwy\.?|highway) 76([^\n])*(E\.?|east) of (Green Canyon|(S\.?|south) Mission)"],
        "CrisisFACTS-001-r6": [],
        "CrisisFACTS-001-r7": [],
        "CrisisFACTS-001-r8": [],
        "CrisisFACTS-001-r9": []
    },


    #Query: What regions have announced a state of emergency?
    "CrisisFACTS-General-q025": {
        "CrisisFACTS-001-r3": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"],
        "CrisisFACTS-001-r4": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"],
        "CrisisFACTS-001-r5": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"],
        "CrisisFACTS-001-r6": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"],
        "CrisisFACTS-001-r7": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"],
        "CrisisFACTS-001-r8": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"],
        "CrisisFACTS-001-r9": [r"state of emergency(\w| )*San Diego|San Diego(\w| )*state of emergency"]
    },

    #Query: What roads are blocked/closed?
    "CrisisFACTS-General-q026": {
        "CrisisFACTS-001-r3": [r"(Highway|hwy\.?) 76"],
        "CrisisFACTS-001-r4": [r"(Highway|hwy\.?) 76"],
        "CrisisFACTS-001-r5": [r"(Highway|hwy\.?) 76"],
        "CrisisFACTS-001-r6": [],
        "CrisisFACTS-001-r7": [],
        "CrisisFACTS-001-r8": [],
        "CrisisFACTS-001-r9": []
    },

    #Query: What roads have been reopend?
    "CrisisFACTS-General-q027": {
        "CrisisFACTS-001-r3": [],
        "CrisisFACTS-001-r4": [],
        "CrisisFACTS-001-r5": [],
        "CrisisFACTS-001-r6": [r"(Highway|hwy\.?) 76"],
        "CrisisFACTS-001-r7": [],
        "CrisisFACTS-001-r8": [],
        "CrisisFACTS-001-r9": []
    },

    #Query: What shelters are open?
    "CrisisFACTS-General-q029": {
        "CrisisFACTS-001-r3": [r"Pala Casino",  r"East Valley Community Center", r"Fallbrook High School"],
        "CrisisFACTS-001-r4": [r"Pala Casino",  r"East Valley Community Center", r"Stagecoach Community Park", r"Del Mar Fairgrounds"],
        "CrisisFACTS-001-r5": [r"Palomar College", r"East Valley Community Center", r"Stagecoach Community Park", r"Del Mar Fairgrounds"],
        "CrisisFACTS-001-r6": [r"Palomar College", r"Del Mar Fairgrounds"],
        "CrisisFACTS-001-r7": [r"Palomar College", r"Bostonia Park (&|and) Recreation Center", r"Del Mar Fairgrounds"],
        "CrisisFACTS-001-r8": [r"Palomar College", r"Del Mar Fairgrounds"],
        "CrisisFACTS-001-r9": [r"Del Mar Fairgrounds"]
    },

    #Query: What warnings are currently in effect?
    "CrisisFACTS-General-q032": {
        "CrisisFACTS-001-r3": [r"red flag"],
        "CrisisFACTS-001-r4": [r"red flag"],
        "CrisisFACTS-001-r5": [r"red flag"],
        "CrisisFACTS-001-r6": [r"red flag"],
        "CrisisFACTS-001-r7": [],
        "CrisisFACTS-001-r8": [],
        "CrisisFACTS-001-r9": [r"red flag"]
    },


    #Query: Where are evacuation centers located?
    "CrisisFACTS-General-q036": {
        "CrisisFACTS-001-r3": [r"11154 CA(\-?| )76",  r"2245 (E\.?|east) Valley Parkway", r"2400 (S\.?|south) Stagecoach"],
        "CrisisFACTS-001-r4": [r"11154 CA(\-?| )76",  r"2245 (E\.?|east) Valley Parkway", r"3420 Camino De Los Coches", r"2260 Jimmy Durante"],
        "CrisisFACTS-001-r5": [r"1140 (W\.?|west) Mission", r"2245 (E\.?|east) Valley Parkway", r"3420 Camino De Los Coches", r"2260 Jimmy Durante"],
        "CrisisFACTS-001-r6": [r"1140 (W\.?|west) Mission", r"2260 Jimmy Durante"],
        "CrisisFACTS-001-r7": [r"1140 (W\.?|west) Mission", r"1049 Bostonia", r"2260 Jimmy Durante"],
        "CrisisFACTS-001-r8": [r"1140 (W\.?|west) Mission", r"2260 Jimmy Durante"],
        "CrisisFACTS-001-r9": [r"2260 Jimmy Durante"]
    },

    #Query: What area has the wildfire burned?
    "CrisisFACTS-Wildfire-q001": {
        "CrisisFACTS-001-r3": [r"4100 (acres|ac)"],
        "CrisisFACTS-001-r4": [r"4100 (acres|ac)"],
        "CrisisFACTS-001-r5": [r"4100 (acres|ac)"],
        "CrisisFACTS-001-r6": [r"4100 (acres|ac)"],
        "CrisisFACTS-001-r7": [r"4100 (acres|ac)"],
        "CrisisFACTS-001-r8": [r"4100 (acres|ac)"],
        "CrisisFACTS-001-r9": [r"4100 (acres|ac)"]
    },

    #Query: Are helocopters present?
    "CrisisFACTS-Wildfire-q003": {
        "CrisisFACTS-001-r3": [r"helicopters"],
        "CrisisFACTS-001-r4": [r"helicopters"],
        "CrisisFACTS-001-r5": [r"helicopters"],
        "CrisisFACTS-001-r6": [r"helicopters"],
        "CrisisFACTS-001-r7": [r"helicopters"],
        "CrisisFACTS-001-r8": [r"helicopters"],
        "CrisisFACTS-001-r9": [r"helicopters"]
    },

    #Query: What is the fire containment level?
    #shifting to mid-day report info - timestamp is 1900 previous day to 1859 current day.  New reports produced at 1900 on containment status of fire.
    "CrisisFACTS-Wildfire-q006": {
        "CrisisFACTS-001-r3": [r"0(%| percent) contain"],
        "CrisisFACTS-001-r4": [r"0(%| percent) contain"],
        "CrisisFACTS-001-r5": [r"20(%| percent) contain"],
        "CrisisFACTS-001-r6": [r"60(%| percent) contain"],
        "CrisisFACTS-001-r7": [r"80(%| percent) contain"],
        "CrisisFACTS-001-r8": [r"92(%| percent) contain"],
        "CrisisFACTS-001-r9": [r"95(%| percent) contain"]
    }
}

def binary_match (gold_facts, top_k_results, request_ID, verb=True):
  '''
  A simple scoring algorithm that uses regexes to find answers to the queries for a given request.  Each fully identified fact provides a point, and the total number of points obtained is returned.

  gold_facts: the dictionary containing the event/day pairs of raw strings to use when searching for answers.
  top_k_results: a string containing the text of the top results from a run, each separated by a newline.
  request_ID: the string identifying the desired event/day pair.
  verb: determines if the matched regexes are listed and the source material printed
  '''
  sum = 0
  for key, fact in gold_facts.items():
    if not fact[request_ID]:
      continue

    #Note that multi-part answers to a query give fractional points that total one if all are found
    weight = 1/len(fact[request_ID])

    for factoid in fact[request_ID]:
      res = re.search(factoid, top_k_results, re.IGNORECASE)
      if res:
        if verb:
          print("found: {}".format(factoid))
          print("Source: {}\n".format(top_k_results[res.start() - 50: res.end() + 50]))
        sum += weight
  return sum